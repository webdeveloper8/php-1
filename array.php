<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Soal 1 </h3>";
    /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
    $kids = array("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"); // Lengkapi di sini
    print_r($kids); echo "<br>";
    $adults = array("Hopper", "Nancy",  "Joyce", "Jonathan", "Murray" );
    print_r($adults);
    echo "<h3> Soal 2</h3>";
    /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: ". count($kids); // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";
    // Lanjutkan

    echo "</ol>";

    echo "Total Adults: ". count($adults); // Berapa panjang array adults
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";
    // Lanjutkan

    echo "<h3>Soal 3</h3>";

    $biodata1[`Nama`] = "Will Byers";
    $biodata2[`Age`] = 12;
    $biodata3[`Aliases`] = "Will the Wise";
    $biodata4[`Status`] = "Alive";
    echo "Name: " . $biodata1[`Nama`];
    echo "<br>Age: " . $biodata2[`Age`];
    echo "<br>Alises: " . $biodata3[`Aliases`];
    echo "<br>Status: " . $biodata4[`Status`];

    

    $biodata1[`Nama`] = "Mike Wheeler";
    $biodata2[`Age`] = 12;
    $biodata3[`Aliases`] = "Dungeon Master";
    $biodata4[`Status`] = "Alive";
    echo "<br><br>Name: " . $biodata1[`Nama`];
    echo "<br>Age: " . $biodata2[`Age`];
    echo "<br>Alises: " . $biodata3[`Aliases`];
    echo "<br>Status: " . $biodata4[`Status`];


    $biodata1[`Nama`] = "Jim Hopper";
    $biodata2[`Age`] = 43;
    $biodata3[`Aliases`] = "Chief Hopper";
    $biodata4[`Status`] = "Deceased";
    echo "<br><br>Name: " . $biodata1[`Nama`];
    echo "<br>Age: " . $biodata2[`Age`];
    echo "<br>Alises: " . $biodata3[`Aliases`];
    echo "<br>Status: " . $biodata4[`Status`];


    $biodata1[`Nama`] = "Eleven";
    $biodata2[`Age`] = 12;
    $biodata3[`Aliases`] = "El";
    $biodata4[`Status`] = "Alive";
    echo "<br><br>Name: " . $biodata1[`Nama`];
    echo "<br>Age: " . $biodata2[`Age`];
    echo "<br>Alises: " . $biodata3[`Aliases`];
    echo "<br>Status: " . $biodata4[`Status`];

    echo "<br><br><br>";

    $tes = [
        ["Will Byers", 12, "Will the Wise", "Alive"],
        ["Mike Wheeler", 12, "Dungeon Master", "Alive"],
        ["Jim Hopper", 43, "Chief Hopper", "Deceased"],
        ["Eleven", 12, "El", "Alive"]
    ];

    echo "<pre>";
    print_r($tes);
    echo "</pre>";

    

    /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"


            Output:
            Array
                (
                    [0] => Array
                        (
                            [Name] => Will Byers
                            [Age] => 12
                            [Aliases] => Will the Wise
                            [Status] => Alive
                        )

                    [1] => Array
                        (
                            [Name] => Mike Wheeler
                            [Age] => 12
                            [Aliases] => Dugeon Master
                            [Status] => Alive
                        )

                    [2] => Array
                        (
                            [Name] => Jim Hooper
                            [Age] => 43
                            [Aliases] => Chief Hopper
                            [Status] => Deceased
                        )

                    [3] => Array
                        (
                            [Name] => Eleven
                            [Age] => 12
                            [Aliases] => El
                            [Status] => Alive
                        )

                )
            
        */
    ?>
</body>

</html>